var restify = require("restify");
var firebase = require("firebase");
var server = restify.createServer();
server.use(restify.CORS());
server.use(restify.bodyParser());
server.use(restify.queryParser());


var config = {
    apiKey: "AIzaSyAqJT7rgRNEghm2BWpBqt-3Yr7yLbO59qU",
    authDomain: "resite-assignment.firebaseapp.com",
    databaseURL: "https://resite-assignment.firebaseio.com",
    projectId: "resite-assignment",
    storageBucket: "resite-assignment.appspot.com",
    messagingSenderId: "223663215556"
}
firebase.initializeApp(config);
 
var database = firebase.database();

server.listen(process.env.PORT ||8080, function(req,err){
  if(!err){
    console.log("The API server running,now");
  }else{
    console.log("The seerver has error, please late try against");
    
  }
});

server.get('/showmenu', function(req, res, next) {
  var id = req.query.id;
  if (id == null){
    id="";
  }
  var result = new Promise(function(resolve, reject) {
  
    var menuRef = 'menus/' + id
    
    var newmenuRef = database.ref(menuRef);
    
    newmenuRef.once('value').then(function(snap) {
      
      var jsondata = snap.val();
      var sizes = snap.numChildren();

      if (jsondata != null) {
          sizes = 1;
        resolve({
          success: true,
          data: jsondata,
          sizes: sizes,
          message:"successful to obtain cooking menu"
        });
      }

      else {
        resolve({
          success: false,
          message: "The menuid cannot found"
        });
      }
    });
  });
  
  result.then(function(value) {
    res.send(value);
    res.end();
  })
})




server.post('/Insertmenu', function(req, res, next) {
    var result = new Promise(function(resolve, reject) {
        if (req.headers['content-type'] != 'application/json') {
            resolve({
                success: false,
                message: "fail to insert the menu to database, please entry suiable syntax"
            });
        }
        else {

            var jsondata = req.body;
            var id = req.query.newMenuID;
            var Menuid;
            var MenuRef;
            var IdsRef = 'Id/';

            database.ref('Id/').once('value').then(function(snap) {
                var menuDatabase = snap.val();
                if (menuDatabase != null) {
                    Menuid = menuDatabase;
                    if (Object.prototype.toString.call(jsondata) === '[object Object]') {
                        var newmenuId = menuDatabase + 1;
                        MenuRef = "menus/" + newmenuId;
                        firebase.database().ref(MenuRef).set(jsondata);
                        firebase.database().ref(IdsRef).set(newmenuId);
                    }
                    else {

                        for (i = 0; i < jsondata.length; i++) {
                            var newmenuId = i + Menuid;
                            MenuRef = "menus/" + newmenuId;
                            firebase.database().ref(MenuRef).set(jsondata[i]);
                            menuDatabase++;
                        }
                        firebase.database().ref(IdsRef).set(menuDatabase);
                    }
                }

                else {

                    if (Object.prototype.toString.call(jsondata) === '[object Object]') {
                        var newmenuId = 0;
                        MenuRef = "menus/" + newmenuId;
                        firebase.database().ref(MenuRef).set(jsondata);
                        newmenuId++;
                        firebase.database().ref(IdsRef).set(newmenuId);
                    }
                    else {
                        Menuid = 0;
                        for (i = 0; i < jsondata.length; i++) {
                            MenuRef = "menus/" + i;
                            firebase.database().ref(MenuRef).set(jsondata[i]);
                            Menuid++;
                        }
                        firebase.database().ref(IdsRef).set(Menuid);
                    }
                }
            });

            resolve({
                success: true,
                message: "successful to insert the menu to database",
                "jsondata": jsondata.length,
                "Menuid": Menuid,
                list:jsondata

            });
        }
    });
    result.then(function(value) {
        res.send(value);
        res.end();
    });
});


server.put("/updatemenu", function(req, res, next){
    var result = new Promise(function(resolve, reject){
        if (req.headers['content-type'] == 'application/json'){
            var id = req.query.newMenuID;
            var jsondata = req.body;
            if (id != null && jsondata != null){

                firebase.database().ref("menus/" + id).update(jsondata);

                resolve({
             
                    success: true,
                    message:"success, update the menu information",
                    id : id,
                    update: jsondata
                });
            }else{
                resolve({
                    success: false,
                    message:"fail to update menu"
                });
            }
        }
    });
    result.then(function(value){
        res.send(value);
        res.end();
    });
});



server.del('/deletemenu' , function(req, res,next){
    var result = new Promise(function (resolve, reject){
       
        var newsMenuID = req.query.newMenuID;
        if (newsMenuID != null){
            
            var menusRef = "menus/" + newsMenuID;

            firebase.database().ref(menusRef).remove();
            resolve({
               
                success: true,
                newsMenuID : newsMenuID,
                message : "sucessful to remove menu"
            });
        }else{
         
            resolve({
                success :false,
                message : "fail to remove men"
            });
        }
    });

    result.then(function(value){
        res.send(value);
        res.end();
    });
});