var app = angular.module('client', []);

 app.controller('menu', ($scope) => {

    $scope.displayuser = () => {
        var url;
    if ($scope.id == null) {
      url = "https://reassignment.herokuapp.com/showmenu";
    }
    else {
      url = "https://reassignment.herokuapp.com/showmenu?newsMenuID=" + $scope.id;
    }
        $.ajax({
                url: url ,
                type:'GET',
                success: (result) =>{
                console.log(result);
        $scope.menuList = result.list;
        $scope.$apply();
    }
    })

}

 })
 
 app.controller('addmenu', function($scope, $rootScope) {
  $rootScope.log = $scope.log;
  $scope.Insertmenu = () => {
    if ($scope.username && $scope.password && $scope.name && $scope.material && $scope.cookingway) {
        var jsondata = {
            "username": $scope.username,
            "password": $scope.password,
            "name": $scope.name,
            "material": $scope.material,
            "cookingway": $scope.cookingway
        };

      $.ajax({
        url: "https://reassignment.herokuapp.com/showmenu",
        type: 'GET',
        success: (result) => {
          for (i = 0; i < result.list.length; i++) {
            if (result.list[i] != null) {
              if ($scope.username == result.list[i].username) {
                alert("This username has been registered , please use other user to submit the cookingway");
                break;
              }
              else {
                if (i == result.list.length-1) {
                  $.ajax({
                      url:"https://reassignment.herokuapp.com/Insertmenu",
                      type:"POST",
                      data:  JSON.stringify(jsondata),
                      processData: false,
                      contentType: 'application/json',
                      dataType: 'json'
                  });
                  console.log(jsondata);
                }
              }          }
          }
          $scope.$apply();
        }
      })
    }
    else {
      alert("Please full fill all data");
    }
  }
});

app.controller('updatemenu', function ($scope,$rootScope) {
  $rootScope.log = $scope.log;
  $scope.updatemenu = () => {
        var jsondata = {
            "id": $scope.id,
            "username": $scope.username,
            "password": $scope.password,
            "name": $scope.name,
            "match": $scope.material,
            "cookingway": $scope.cookingway
        };
            if($scope.id!=null){
                var url = "https://reassignment.herokuapp.com/updatemenu?newsMenuID="+ $scope.id;
            $.ajax({
            url:url,
            type: "PUT",
            data:  JSON.stringify(jsondata),
            processData: false,
            contentType: 'application/json',
            dataType: 'json'
        })
              alert("sucessful update to menu")
            }else{
                alert("Please input id");
            }
             
    }
 
})


app.controller('deletemenu', function ($scope,$rootScope) {
    $rootScope.log = log;
    $scope.deletmenu = () => {
        if($scope.id != null){
            var url = "https://assignment2and3.herokuapp.com/removeuser?newuserID=" + $scope.id;
                $.ajax({
                    url:url,
                type:"DELETE"
            });
            alert("sucessful delete the user data")
        }
        else {
            alert("Please input the id number to delete the user for database");
        }
    }
});

var log;
app.controller('loginmenu', function($scope, $rootScope) {
  $rootScope.log = log;
  $scope.logins = () => {
    $.ajax({
      url: "https://reassignment.herokuapp.com/showmenu",
      type: 'GET',
      success: (result) => {
        if ($scope.users && $scope.passwords) {
          for (i = 0; i < result.list.length; i++) {
            if (result.list[i] != null) {
              if (result.list[i].username == $scope.users) {
  
                if (result.list[i].password == $scope.passwords) {
               
                  log = true;
                  $rootScope.log = log;
               
                  $rootScope.users1 = result.list[i].name;
                  break;
                }
                else{
                        alert('password wrong');
                        break;
                }
              }
              else {
                if(i == result.list.length){
                        alert("no data");
                        break;
                }
              }
            }
          }
        }
        else {
      alert("please provide suitable password and username");
        }
         $scope.$apply();
      }
    })
  }
});